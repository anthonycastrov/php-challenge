<?php

namespace App;


class Contact
{
    protected $names = [
        'Anthony',
        'Johan'
        ];

    public $foundContact = false;
    public $name;

	function __construct($name)
	{
        if(in_array($name, $this->names, true)){
            $this->foundContact = true;
            $this->name = $name;
        }
	}
}