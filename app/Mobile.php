<?php

namespace App;

use App\Interfaces\CarrierInterface;
use App\Services\ContactService;


class Mobile
{

    protected $provider;

    function __construct(CarrierInterface $provider)
    {
        $this->provider = $provider;
    }


    public function makeCallByName($name = '')
    {
        if (empty($name)) return;

        $contact = ContactService::findByName($name);

        if (!$contact->foundContact) return 'contact not found';

        $this->provider->dialContact($contact);

        return $this->provider->makeCall();
    }


    public function sendSMS($number, $body)
    {
        $validNumber = ContactService::validateNumber($number);

        if (!$validNumber) return 'The number is invalid';

        $this->provider->setSMSNumber($number);
        $this->provider->setSMSBody($body);

        return $this->provider->sendSMS();
    }


}
