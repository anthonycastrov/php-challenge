<?php

namespace App\Providers;

use App\Call;
use App\Sms;
use App\Contact;
use App\Interfaces\CarrierInterface;



class FirstProvider implements CarrierInterface
{

    protected $numberFrom = '+16102980114';
    protected $number;
    protected $body;

    function __construct()
    {
    }

    public function dialContact(Contact $contact)
    {
        // TODO: Implement dialContact() method.
    }

    public function makeCall(): Call
    {
        return new Call();
    }

    public function setSMSNumber($number)
    {
        $this->number = $number;
    }

    public function setSMSBody($body)
    {
        $this->body = $body;
    }

    public function sendSMS(): Sms
    {
        $response = true;
        return new Sms($this->number, $this->body, $response);
    }
}
