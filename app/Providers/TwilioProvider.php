<?php

namespace App\Providers;

use App\Call;
use App\Sms;
use App\Contact;
use App\Interfaces\CarrierInterface;
use Twilio\Rest\Client;


class TwilioProvider implements CarrierInterface
{

    protected $sid = 'ACbce6bdf5896683193646b1dab4ca6203';
    protected $token = 'a4a1c42baac9c43d44f8475dd31b7d77';
    protected $numberFrom = '+16102980114';
    protected $client;
    protected $number;
    protected $body;

    function __construct()
    {
    }

    private function initClient()
    {
        $this->client = new Client($this->sid, $this->token);
    }

    public function dialContact(Contact $contact)
    {
        // TODO: Implement dialContact() method.
    }

    public function makeCall(): Call
    {
        return new Call();
    }

    public function setSMSNumber($number)
    {
        $this->number = $number;
    }

    public function setSMSBody($body)
    {
        $this->body = $body;
    }

    public function sendSMS(): Sms
    {
        $this->initClient();
        $response = $this->client->messages->create(
            $this->number,
            [
                'from' => $this->numberFrom,
                'body' => $this->body
            ]
        );
        return new Sms($this->number, $this->body, ($response->status === 'queued'));
    }
}
