<?php

namespace App\Services;

use App\Contact;


class ContactService
{

    public static function findByName(string $name): Contact
    {
        return new Contact($name);
    }

    public static function validateNumber(string $number): bool
    {
        return (preg_match('/^[0-9+]+$/', $number));
	}
}