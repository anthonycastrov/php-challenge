<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Mockery as m;

use App\Mobile;
use App\Sms;
use App\Call;

use App\Providers\FirstProvider;
use App\Providers\SecondProvider;
use App\Providers\TwilioProvider;

class MobileTest extends TestCase
{

    /**
     * @test
     * Fix any errors and add the necessary to make the test work and commit it
     */
	public function it_returns_null_when_name_empty()
	{
		$mobile = new Mobile(new FirstProvider());
		$this->assertNull($mobile->makeCallByName(''));
	}

    /**
     * @test
     * Add a test for the method makeCallByName passing a valid contact, mock up any hard dependency and add the right assertions
     */
    public function it_returns_a_instance_of_call_when_name_is_valid()
	{
		$mobile = new Mobile(new FirstProvider());
		$call = $mobile->makeCallByName('Anthony');
        $this->assertInstanceOf(Call::class, $call);
	}

    /**
     * @test
     * Add the necessary code in the production code to check when the contact is not found and add another test to test that case
     * Valid names -> Anthony, Johan
     */
    public function it_return_string_message_when_name_is_not_found()
	{
		$mobile = new Mobile(new FirstProvider());
		$call = $mobile->makeCallByName('Luke');
        $this->assertEquals('contact not found', $call);
	}

    /**
     * @test
     * Add your own logic to send a SMS given a number and the body, the method should validate the number using the validateNumber method from ContactService and using the provider property’s methods
     * Valid number need contain a +
     */
    public function it_returns_a_instance_of_sms_when_number_is_valid()
	{
		$mobile = new Mobile(new FirstProvider());
		$sms = $mobile->sendSMS('+959551951', 'Hello there!');
        $this->assertInstanceOf(Sms::class, $sms);
	}

    /**
     * @test
     * Add your own logic to send a SMS given a number and the body, the method should validate the number using the validateNumber method from ContactService and using the provider property’s methods
     * Valid number need contain a +
     */
    public function it_returns_a_string_message_when_number_is_invalid()
	{
		$mobile = new Mobile(new FirstProvider());
		$sms = $mobile->sendSMS('3SD5FG3PLE', 'Hello there!');
        $this->assertEquals('The number is invalid', $sms);
	}

    /**
     * @test
     * Can you add support for two mobile carriers? How would you accomplish that?
     */
    public function it_returns_a_instance_of_call_when_name_is_valid_using_a_second_provider_with_same_interface()
    {
        $mobile = new Mobile(new SecondProvider());
        $call = $mobile->makeCallByName('Anthony');
        $this->assertInstanceOf(Call::class, $call);
    }

    /**
     * @test
     * Create a new integration with an external service like Twilio to send and track an SMS.
     */
    public function it_returns_a_instance_of_sms_when_the_sms_of_twilio_integration_is_sent()
    {
        $mobile = new Mobile(new TwilioProvider());
		$sms = $mobile->sendSMS('+51944344718', 'Hello there!');
        $this->assertInstanceOf(Sms::class, $sms);
    }

}
